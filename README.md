# Latihan #

1. Buat folder project baru (latihan-java)
2. Copas pom.xml dan sesuaikan groupId dan artifactId
3. Buat package baru di dalam src/main/java
4. Buat kode program yang menggunakan class Kalkulator
5. Amati bahwa terjadi error, class Kalkulator tidak ditemukan
6. Tambahkan dependensi project belajar-java
7. Install project belajar-java ke repository local
8. Reload project latihan-java supaya membaca ulang repo local
9. Import class Kalkulator